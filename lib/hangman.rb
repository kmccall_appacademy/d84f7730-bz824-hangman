class Hangman
attr_reader :guesser, :referee, :board
  def initialize(players)
  	@guesser = players[:guesser]
  	@referee = players[:referee]
  	@times = 0
  end

  def play
    self.setup
    while @board.include?(nil)
      p self.display
      self.take_turn
    end
    puts "The secret word was #{@board.join('').chomp}!"
    puts "You got it in #{@times} times!"
  end

  def setup
    secret_word_length = @referee.pick_secret_word
    @guesser.register_secret_length(secret_word_length)
    @board = Array.new(secret_word_length)
  end

  	def take_turn
    	@times += 1
    	guessed_chr = @guesser.guess(@board)
    	indexes = @referee.check_guess(guessed_chr)
    	update_board(indexes, guessed_chr)
    	@guesser.handle_response(guessed_chr, indexes)
  	end

  	def update_board(indexes, chr)
    	indexes.each{|idx| @board[idx] = chr}
  	end

  	def won?
  		@board.all?
  	end

end

class HumanPlayer
	def initialize(dictionary)
    	@dictionary = dictionary
  	end

	def register_secret_length(length)
   		 @dictionary = self.candidate_words.select{|word| word.length == length}
  	end

	def guess
		p board
		puts "Make guess:"
		guess=gets.chomp
	end

	def handle_response(chr, index)
    	@dictionary = self.candidate_words.select{|word| check_guess(chr, word) == index}
  	end

	def pick_secret_word
    	@word = @dictionary.sample.chomp
    	@word.length
 	end

	def candidate_words
    	@dictionary
  	end

    def check_guess(letter, word = @word)
   	 	idxes = []
    	word.split('').each_with_index do |chr, idx|
      	idxes << idx if letter == chr
    	end
    	idxes
  	end

	def require_secret
		puts "What word were you thinking off?"
		gets.chomp
	end
end

class ComputerPlayer

	def self.player_with_dict_file(dict_file_name)
		ComputerPlayer.new(File.readlines(dict_file_name).map(&:chomp))
	end

	attr_reader :candidate_words

	def initialize(dictionary)
		@dictionary = dictionary
	end

	def pick_secret_word
		@secret_word = @dictionary.sample

		@secret_word.length
	end

	def check_guess(guess)
		response = []
		@secret_word.split("").each_with_index do |letter, index|
			response << index if letter == guess
		end
		response
	end

	def register_secret_length(length)
		#begin to play again; reset candidate_words
		@candidate_words = @dictionary.select {|word| word.length == length}
	end

	def guess(board)
		freq_table = freq_table(board)
		most_frequent_letters = freq_table.sort_by {|letter, count| count}
		letter, _ = most_frequent_letters.last
		letter
	end

	def handle_response(guess, response_indices)
		@candidate_words.reject! do |word|
			should_delete = false
			word.split("").each_with_index do |letter, index|
				if (letter == guess) && (!response_indices.include?(index))
					should_delete = true
					break
				elsif (letter != guess) && (response_indices.include?(index))
					should_delete = true
					break
				end
			end
			should_delete
		end
	end

	def require_secret
		@secret_word
	end

	private

	def freq_table(board)
		freq_table = Hash.new(0)
		@candidate_words.each do |word|
			board.each_with_index do |letter, index|
				freq_table[word[index]] +=1 if letter.nil?
			end
		end
		freq_table
	end
end
